FROM harbor.sun.com/library/tomcat:9.0.65-jre8

ARG NAME
ARG VERSION
ARG JAR_FILE

LABEL name=$NAME \
      version=$VERSION

# 设定时区
ENV TZ=Asia/Shanghai
RUN set -eux; \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime; \
    echo $TZ > /etc/timezone


COPY target/${JAR_FILE} /usr/local/tomcat/webapps/
